/**
 * Forest Watch Bot
 * 
 * Retrieves data from GLAD/UMD, updates a Twitter feed
 * @version 2.7.5
 * @author Vera Rubin
 * @license MIT
 * @repository http://gitlab.com/wishiwasrubin/fwbot
 */
class ForestWatchBot {

    /**
     * Init the bot
     * @param {object} config Object containing bot configurable parameters
     */
    constructor(config) {
        // HTTP module used to maki API calls
        this.http = require('http');

        // FS to load and save Bot memory and maps
        this.fs = require('fs');

        // JIMP to make maps
        this.jimp = require('jimp');
        this.map = config.map;

        this.memoryUpdate = config.memory.update;
        this.memoryLocation = config.memory.file;
        // Read memory
        this.memoryRead()
            .then((memory) => {
                this.memory = memory;
            })
            .catch((error) => {
                console.log(error);
            });

        // Load Twitter API
        var Twitter = require('twitter');
        this.twitterAPI = new Twitter({
            consumer_key: config.twitter.consumerKey,
            consumer_secret: config.twitter.consumerSecret,
            access_token_key: config.twitter.accessTokenKey,
            access_token_secret: config.twitter.accessTokenSecret
        });

        // Save status configuration
        this.statusArguments = config.status;
        this.status = {};

        // Run
        this.init();
    }

    /**
     * Bot routine
     */
    init () {
        // Fetch latest alerts
        this.http.get('http://production-api.globalforestwatch.org/glad-alerts/latest', (response) => {
            response.on('data', (alert) => {
                alert = JSON.parse(alert);
                let gladDate = alert.data[0].attributes.date;
                let gladTime = new Date(gladDate).getTime();

                if (gladTime > this.memory.previous.time) {
                    // Alert is new, fetch all GLADs
                    let datePeriod = this.memory.previous.date + ',' + new Date().toISOString().split('T')[0];

                    this.fetchGLAD(datePeriod)
                        .then((response) => {
                            this.twitterUpdate(response.alerts);

                            this.status = {
                                date: new Date(),
                                dateOfLatest: gladDate,
                                dateOfPrevious: this.memory.previous.date,
                                result: response.alerts.length + ' new alerts. Fetched ' + response.fetched + ' countries in ' + response.time + ' seconds'
                            }
                            this.consoleLog('GLAD API fetched at ' + new Date());
                            this.consoleLog(this.status);
                        })
                        .catch((error) => {
                            console.log(error);
                        });
                    
                    // Save to memory
                    this.memorySave({
                        previous: {
                            date: gladDate,
                            time: gladTime
                        }
                    });

                    // Log in console
                    this.consoleLog('New alert received. Fetching API.');
                }

                else {
                    this.status = {
                        date: new Date(),
                        dateOfLatest: gladDate,
                        dateOfPrevious: this.memory.previous.date,
                        result: 'No new GLAD alerts'
                    }
                    this.consoleLog('GLAD API fetched at ' + new Date());
                    this.consoleLog(this.status);
                }
            });

            response.on('error', (error) => {
                console.log(error);
            })
        });
    }

    /**
     * Fetchs GLAD API from given period to every country
     * @param {string} datePeriod Period range
     * @returns {array}
      */
    fetchGLAD (datePeriod) {
        return new Promise((resolve, reject) => {
            let API = 'http://production-api.globalforestwatch.org/glad-alerts/admin/';
            
            let getCountry = require('country-list'); 
            let getCountryISO3 = require('country-iso-2-to-3');
            
            let alerts = new Array();
            let start = new Date().getTime();

            // Get every GLAD covered country
            getCountry.getCodes().forEach((country, index) => {
            // Add a delay to not overflow the server
            setTimeout(() => {
                this.http.get(API + getCountryISO3(country) + '?period=' + datePeriod, (GLAD) => {
                    let result;
                    
                    // Parse result data buffer to JSON
                    GLAD.on('data', (data) => {
                        try {
                            result = JSON.parse(data);
                        }
                        
                        catch (error) {
                            result = false;
                        }
                    });
                    
                    // Process values
                    GLAD.on('end', () => {
                        if (result && result.data && result.data.attributes && result.data.attributes.value !== 0) {
                            let values = {
                                area: result.data.attributes.value*30*30,
                                country: getCountry.getName(country)
                            };

                            // Push values to alerts array
                            alerts.push(values);
                            // Log result to console if specified
                            this.consoleLog(values)
                        }

                        if (index >= getCountry.getCodes().length - 1) {
                            resolve({
                                alerts: alerts,
                                fetched: index + 1,
                                time: (new Date().getTime() - start) / 1000,
                            });
                        }
                    })

                    GLAD.on('error', (error) => {
                        reject(error);
                    });
                });
            }, 600 * (index + 1));
            });
        });
    }
    

    /**
     * Reads memory file in a Promise based fashion
     * @returns {Promise object}
     */
    memoryRead () {
        return new Promise((resolve, reject) => {
            this.fs.readFile(this.memoryLocation, (error, data) => {
                if (error) {
                    reject(error);
                }

                else {
                    try {
                        data = JSON.parse(data);
                        resolve(data);
                    }
    
                    catch (error) {
                        reject(error);
                    }
                }
            });
        });    
    }

    /**
     * Save data to bot memory
     * @param {object} memoryData Data to be stored in memory
     * @returns {Promise object}
     */
    memorySave (memoryData) {
        if (this.memoryUpdate) {
            return new Promise((resolve, reject) => {
                this.fs.writeFile(this.memoryLocation, JSON.stringify(memoryData, null, '\t'), (error, data) => {
                    if (error) {
                        reject(error);
                    }
    
                    else {
                        resolve(data);
                    }
                });
            });
        }
    }

    /**
     * Updates Twitter feed if the bot is told to
     * @param {array} alerts Array with resulting alerts objects from GLAD API  
     */
    twitterUpdate (alerts) {
        if (this.statusArguments.twitterUpdate !== false) {
            let totalDeforestation = 0;

            // Make map of total area
            alerts.forEach((alert, index) => {
                totalDeforestation += alert.area;

                if (index >= alerts.length - 1) {
                    this.twitterMap(totalDeforestation)
                }
            });
        }
    }

     /**
     * Composes image map over given city comparing areas
     * @param {int} totalDeforestation 
     * @returns {Promise object}
     */
    makeMap (totalDeforestation) {
        return new Promise((resolve, reject) => {
            var jimp = this.jimp;
            
            let area = Math.sqrt(totalDeforestation);
            // Randomly choose a map from the given array
            let map = this.map[Math.floor(Math.random()*this.map.length)];
            this.consoleLog('Drawing map of ' + map.name + ' with ' + area + 'm^2 comparison.');

            jimp.read('map/' + map.file)
                .then((image) => {
                    new jimp(area, area, '#ff400060', (error, lostArea) => {
                        image.composite(lostArea, 200, 200);
                        image.getBufferAsync(jimp.MIME_JPEG).then((image) => {
                            resolve({
                                image: image, 
                                city: map.name
                            });
                        });
                    });
                })

                .catch((error) => {
                    reject(error);
                });
        });
    }

    /**
     * Updates twitter feed with Map image
     * @param {int} totalDeforestation
     */
    twitterMap (totalDeforestation) {
        this.makeMap(totalDeforestation) 
            .then((map) => {
                this.twitterAPI.post('media/upload', {media: map.image}, (error, data, response) => {
                    if (error) {
                        console.log(error);
                    }

                    else {
                        this.twitterAPI.post('statuses/update', {
                            status: totalDeforestation / 10000 + ' hectares of forest lost between ' + this.memory.previous.date + ' and ' + new Date().toISOString().split('T')[0] + '. Area comparison to the city of ' + map.city,

                            media_ids: data.media_id_string
                        }, (error, data, response) => {
                            if (error) {
                                console.log(error);
                            }

                            else {
                                this.consoleLog('Twitter feed successfully updated.');
                            }
                        });
                    }
                });
            });
    }

    /**
     * Logs in to console if the bot is told to
     * @param {any} message
     */
    consoleLog(message) {
        if (this.statusArguments.consoleLog !== false) {
            console.log(message);
        }
    }

    /**
     * Return status of a bot instance
     * @param {object} instance Instance of a previous run
     * If left empty it will return current instance status
     * @param {callback} callback Function
     */
    getStatus(instance = {}, callback) {
        if (instance.status) {
            callback(instance.status);
        }

        else {
            callback(this.status);
        }
    }

}

module.exports = ForestWatchBot;
