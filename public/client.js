// client-side js
// run by the browser each time your view template is loaded

function readStatus() {
	$.get('/status', {}, (data) => {
		console.log(data);
		$('#date').text(new Date(data.date));
		$('#dateOfGLAD').text(data.dateOfLatest);
		$('#dateOfPreviousGLAD').text(data.dateOfPrevious);
		$('#result').text(data.result);
		$('#retrievedAt').text(new Date());
	});
}

readStatus();
setInterval(readStatus, 300000);
