# Forest Watch Bot

Serve a Twitter feed with near real time deforestation data.  
Bot feed at https://twitter.com/ForestWatchBot  
Running Bot status at https://forest-watch-bot.glitch.me

## How it works

This bot fetches Global Forest Watch API listening for new GLAD alerts. When GFW retrieves a new latest alert date, it crawls the GFW API again listening for GLAD alerts by country, with a period range since the previous update and the current date, updating the feed with a new tweet containing the total area lost in a map comparison.

## Maps and area comparison
>**This project is currently looking for more maps!** 
In order to submit a new map you can fork this project, add your map image to the `map` folder and create a Pull Request, specifying the resolution of the map image in your commit.
Please submit maps of fairly known cities and *pictures on the public domain only*.

Maps are just satellite pictures of a fairly big city over which is placed an square the same size of the deforestated area.

Currently there are only 5 maps, all of them with a resolution of 30m^2 per pixel, which is the same resolution that the GLAD alerts have, this same resolution ratio is used in order to make it easier to render the final area comparison, however it could be possible to add new maps with different resolutions and specify their resolution to the Bot so it can calculate the area comparison in a responsive way.

## About GLAD alerts and GFW

*Global Land Analysis & Discovery* (GLAD) is a laboratory at the University of Maryland. In collaboration with Global Forest Watch they provide a satellite based alert system to track tree cover loss in 22+ countries around the tropics. You can read more information about this alerts at [globalforestwatch.org](http://data.globalforestwatch.org/datasets/194662b1470e4c5f81aa370395c75485). This source of alerts is what *Forest Watch Bot* uses. 

Global Forest Watch provides different sets of data to visualize tree and forest surface evolution in more places and in more depth, gaining knowledge about not only deforestation but also forest growth and evolution. The purpose of this bot it's not to provide such information, it is to raise awareness about deforestation.

This project is not oficially afiliated to nor patroned by either Global Forest Watch or the University of Maryland.
