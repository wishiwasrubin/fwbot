var express = require('express');
var APP = express();

// Load env vars
require('dotenv').config();

// Start bot
var ForestWatchBot = require('./bot');
var Bot = new ForestWatchBot({
    memory: {
        file: 'memory.json',
        update: process.env.memoryUpdate
    },

    map: [
        { file: 'bm.jpg', name: 'Baltimore' },
        { file: 'ny.jpg', name: 'New York' },
        { file: 'rj.jpg', name: 'Rio de Janeiro' },
        { file: 'sp.jpg', name: 'Sao Paulo' },
        { file: 'ws.jpg', name: 'Warsaw'},
    ],

    twitter: {
        consumerKey: process.env.consumerKey,
        consumerSecret: process.env.consumerSecret,
        accessTokenKey: process.env.accessTokenKey,
        accessTokenSecret: process.env.accessTokenSecret
    },
    
    status: {
        // Tell the bot to not update the feed
        twitterUpdate: process.env.twitterUpdate,

        // Tell the bot to console.log 
        consoleLog: process.env.consoleLog
    }
});

// Status
APP.use(express.static('public'));
APP.get('/', (request, response) => {
    response.sendFile(__dirname + '/views/index.html');
});

APP.get('/status', (request, response) => {
    Bot.getStatus(Bot, (status) => {
        response.send(status);
    });
});

var listener = APP.listen(process.env.PORT, () => {
    console.log('Forest Watch Bot server is listening on port ' + listener.address().port);
});
